package com.example.demo.configuration;


import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@org.springframework.context.annotation.Configuration
public class DatasourceConfiguration {

    @Bean
    public DataSource dataSource (){
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        builder.setType(EmbeddedDatabaseType.H2)
                .addScripts("classpath:/static/table.sql","classpath:/static/data.sql");
        return builder.build();
    }

}
