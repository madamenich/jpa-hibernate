package com.example.demo.controller;

import com.example.demo.repository.BookRepository;
import com.example.demo.repository.BookRepositoryTest;
import com.example.demo.repository.model.joined_subclass.ITETeacher;
import com.example.demo.repository.model.joined_subclass.ShortCourseTeacher;
import com.example.demo.repository.model.joined_subclass.Teacher;
import com.example.demo.repository.model.mapped_supper_class.FullTimeStaff;
import com.example.demo.repository.model.mapped_supper_class.PartTimeStaff;
import com.example.demo.repository.model.single_table.Circle;
import com.example.demo.repository.model.single_table.Rectangle;
import com.example.demo.repository.model.table_per_class.Car;
import com.example.demo.repository.model.table_per_class.MotorBike;
import com.example.demo.repository.model.table_per_class.Vehicle;
import com.example.demo.repository.relationship.Article;
import com.example.demo.repository.relationship.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@RestController
@RequestMapping("/test")
public class TestController {
    @Autowired
  BookRepository bookRepository;
    @PersistenceContext
    EntityManager e;


  @GetMapping("")
  @Transactional
    public String add(){

//      e.persist(new Circle(12.03));
//      e.persist(new Rectangle(12,13));
//     e.persist(new FullTimeStaff("dara","Android"));
//     e.persist(new PartTimeStaff("siekny","React"));
//     e.persist(new Teacher("Nich"));
//     e.persist(new ITETeacher(8,"Spring and Angular"));
//     e.persist(new ShortCourseTeacher("J2SE"));

      e.persist(new Vehicle("prius"));
      e.persist(new Car("camery","Toyota"));
      e.persist(new MotorBike("Scoopy","Honda"));
        e.persist(new Category(1,"Novel"));
      e.persist(new Category(2,"Fiction"));
      e.persist(new Article(1,"Love is in the air",new Category(1)));


    return "done";
    }
}
