package com.example.demo.controller;

import com.example.demo.repository.BookRepositoryTest;
import com.example.demo.repository.model.Book;
import com.example.demo.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/books")
public class BookController {
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private BookRepositoryTest bookRepositoryTest;
    @GetMapping("")
    List<Book> findAll(){
        System.out.println(bookRepositoryTest.find(1));
        System.out.println(bookRepositoryTest.findTitle());
        return bookRepository.findAll();
    }




}
