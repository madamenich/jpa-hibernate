package com.example.demo.repository;

import com.example.demo.repository.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepositoryTest extends JpaRepository<Book,Integer>{
    @Query(name = "findOne")
    public String find(@Param("id") int id);
    @Query(name="getTitle")
     public String findTitle();
}
