package com.example.demo.repository.model.joined_subclass;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(referencedColumnName = "t_id")
public class ITETeacher extends Teacher {
    private int generation;
    private String advancedSubject;

    public ITETeacher( int generation, String advancedSubject) {

        this.generation = generation;
        this.advancedSubject = advancedSubject;
    }

    public ITETeacher() {
    }
}
