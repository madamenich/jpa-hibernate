package com.example.demo.repository.model.joined_subclass;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  int t_id;
    private String name;

    public Teacher(String name) {
        this.name = name;
    }

    public Teacher() {
    }
}
