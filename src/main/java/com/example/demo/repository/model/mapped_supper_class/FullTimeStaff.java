package com.example.demo.repository.model.mapped_supper_class;

import javax.persistence.Entity;

@Entity
public class FullTimeStaff extends Staff {
    private String fulltimeTeam;

    public FullTimeStaff(String name, String fulltimeTeam) {
        super(name);
        this.fulltimeTeam = fulltimeTeam;
    }

    public FullTimeStaff() {
    }
}
