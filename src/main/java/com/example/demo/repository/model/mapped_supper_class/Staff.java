package com.example.demo.repository.model.mapped_supper_class;

import javax.persistence.*;

@MappedSuperclass
public class Staff {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  int id;
    private String name;

    public Staff(String name) {
        this.name = name;
    }

    public Staff() {
    }
}
