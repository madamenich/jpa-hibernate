package com.example.demo.repository.model;

import javax.persistence.*;

@Entity(name = "book")
@Table(name = "tb_book")
@NamedQuery(query="select b.name from book as b where b.id = :id",name = "findOne")
@NamedNativeQuery(query = "SELECT b.title FROM tb_book b  where b.id=2",name = "getTitle")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "title")
    private String name;
    @Column
    private String description;

    public Book() {
    }

    public Book(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
