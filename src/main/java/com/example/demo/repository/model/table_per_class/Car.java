package com.example.demo.repository.model.table_per_class;

import javax.persistence.Entity;

@Entity
public class Car extends Vehicle{
    private String manufacturedBy;

    public Car(String modelName, String manufacturedBy) {
        super(modelName);
        this.manufacturedBy = manufacturedBy;
    }

    public Car() {
    }
}
