package com.example.demo.repository.model.embedded;

import javax.persistence.Embeddable;

@Embeddable
public class Address {
    private String roadName;
    private String district;

    public Address() {
    }

    public String getRoadName() {
        return roadName;
    }

    public void setRoadName(String roadName) {
        this.roadName = roadName;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
}
