package com.example.demo.repository.model.joined_subclass;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(referencedColumnName = "t_id")
public class ShortCourseTeacher extends Teacher{
   private String subject;

    public ShortCourseTeacher(String subject) {

        this.subject = subject;
    }

    public ShortCourseTeacher() {
    }
}
