package com.example.demo.repository.model.table_per_class;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Vehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String modelName;

    public Vehicle(String modelName) {
        this.modelName = modelName;
    }

    public Vehicle() {
    }
}
