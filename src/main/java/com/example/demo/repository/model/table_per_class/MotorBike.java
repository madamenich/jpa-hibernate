package com.example.demo.repository.model.table_per_class;

import javax.persistence.Entity;

@Entity
public class MotorBike  extends Vehicle{

    private String company;
    public MotorBike() {
    }
    public MotorBike(String modelName, String company) {
        super(modelName);
        this.company = company;
    }


}
