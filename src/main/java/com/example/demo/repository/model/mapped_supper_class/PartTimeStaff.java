package com.example.demo.repository.model.mapped_supper_class;

import javax.persistence.Entity;

@Entity
public class PartTimeStaff  extends Staff{
    private String partTimeTeam;

    public PartTimeStaff(String name, String partTimeTeam) {
        super(name);
        this.partTimeTeam = partTimeTeam;
    }

    public PartTimeStaff() {
    }
}
