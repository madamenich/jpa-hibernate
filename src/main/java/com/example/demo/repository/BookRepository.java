package com.example.demo.repository;

import com.example.demo.repository.model.Book;
import com.example.demo.repository.model.single_table.Circle;
import com.example.demo.repository.model.single_table.Rectangle;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class BookRepository {

  @PersistenceContext
  EntityManager e;
    public List<Book> findAll(){
        CriteriaBuilder criteriaBuilder = e.getCriteriaBuilder();
        CriteriaQuery<Book> query = criteriaBuilder.createQuery(Book.class);
        Root<Book> bookRoot = query.from(Book.class);
        query.select(bookRoot);
        return e.createQuery(query).getResultList();
    }

    @Transactional
    public void save(){
        e.persist(new Circle(12.03));
        e.persist(new Rectangle(12,13));
    }
}
