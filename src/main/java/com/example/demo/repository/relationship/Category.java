package com.example.demo.repository.relationship;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class Category {
    @Id
    private int id;
    private String name;

    public Category() {
    }

    public Category(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Category(int id) {
        this.id = id;
    }
}
