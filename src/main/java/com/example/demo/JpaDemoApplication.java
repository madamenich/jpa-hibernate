package com.example.demo;

import com.example.demo.repository.BookRepositoryTest;
import com.example.demo.repository.model.single_table.Circle;
import com.example.demo.repository.model.single_table.Rectangle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;


@SpringBootApplication
public class JpaDemoApplication {
    @PersistenceContext
    EntityManager entityManager;
    public static void main(String[] args) {
        JpaDemoApplication jpa = new JpaDemoApplication();

        SpringApplication.run(JpaDemoApplication.class, args);

    }



}
